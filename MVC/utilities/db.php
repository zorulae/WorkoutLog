<?php
//singleton
class Db {
    
    private static $db;

    public static function init() 
    {
        if (!self::$db) { 
            try 
            {
                $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
                self::$db = new PDO($dsn, DB_USER, DB_PASSWORD);
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            } 
            catch (PDOException $exception) 
            {
                die('Connection error: ' . $exception->getMessage());
            }
            return self::$db;
        }
    }
}