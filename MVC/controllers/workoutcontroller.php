<?php

class WorkoutController extends Controller
{
    public function __construct($model, $action)
    {
        parent::__construct($model, $action);
        $this->__setModel($model);
    }

    public function index()
    {
        try
        {
            $workouts = $this->_model->getNWorkouts(7);
            $this->_view->set('workouts', $workouts);
            $this->_view->set('title', 'Index');
            return $this->_view->output();
        }
        catch(Exception $e)
        {
            echo "Application error:" . $e->getMessage();
        } 
    }

    public function edit($workoutID)
    {

        try
        {
            $workout = $this->_model->getWorkoutById((int)$workoutID);

            if($workout)
            {
                $this->_model->setID($workoutID);

                $this->_view->set('title', 'Edit your workout');
                $this->_view->set('date', $workout['Date']);
                $this->_view->set('workoutID', $this->_model->getID());
                $this->_view->set('workoutdata', $workout['TrainingData']);
            }
            else // XXX is this even neccessary and can you even test this?
            {
                $this->_view->set('title', 'Invalid ID');
                $this->_view->set('noWorkout', true);
            }

            if(isset($_POST['editFormSubmit']))
            {
                $workoutData = isset($_POST['entry']) ? trim($_POST['entry']) : NULL;
                $this->_model->setWorkoutData($workoutData);
                $this->_model->updateWorkoutData();

                $this->_setView('success');
                $this->_view->set('title', 'Workout successfully updated!');
                $this->_view->set('workoutdata', $this->_model->getWorkoutData());
            }

            return $this->_view->output();
        }
        catch (Exception $e)
        {
            echo "Application error:" . $e->getMessage();
        }
    }

    public function save()
    {
        try
        {
            $errors = array();
            $check = true;
            $this->_view->set('title', 'Add your workout');

            if (isset($_POST['addFormSubmit'])) 
            {
                $workoutData = isset($_POST['entry']) ? trim($_POST['entry']) : null;

                if (empty($workoutData)) //HTML5 does the catching, should i let it?
                {
                    $check=false;
                    array_push($errors, "Data is required");
                }
                if(!$check)
                {
                    $this->_view->set('title', 'Invalid form data!');
                    $this->_view->set('errors', $errors);
                    $this->_view->set('formData', $_POST);
                    return $this->_view->output();
                }
            
                $this->_model->setWorkoutData($workoutData);
                $this->_model->saveWorkout();

                $this->_setView('success');
                $this->_view->set('title', 'Success!');
                $this->_view->set('workoutdata', $this->_model->getWorkoutData());
            }
        }
        catch(Exception $e)
        {
            //$this->_setView('index'); //FIXME gonna mess up the main index, will it return the same page with different values?
            $this->_view->set('title', 'There was an error saving the data!');
            $this->_view->set('formData', $_POST);
            $this->_view->set('saveError', $e->getMessage());
        }
        
        return $this->_view->output();
    }

    public function delete($workoutID)
    {
        $this->_model->setID($workoutID);
        $this->_view->set('title', 'Delete workout');

        if (isset($_POST['deleteSubmit']))
        {
            $this->_model->deleteWorkout();

            $this->_setView('success');
            $this->_view->set('title', 'Workout deleted!');
        }

        return $this->_view->output();
    }
}