<?php

class Controller
{
    protected $_model;
    protected $_controller;
    protected $_action;
    protected $_view;
    protected $modelBaseName;

    public function __construct($model, $action)
    {
        $this->_controller = ucwords(__CLASS__); // This'll just keep the track of the current controller
        $this->_action = $action;
        $this->_modelBaseName = $model;
    
        $this->_view = new View(HOME . DS . 'views' . DS . strtolower($this->_modelBaseName) 
        . DS . $this->_action . '.tpl');
    }

    protected function __setModel($modelName)
    {
        $modelName .=  'Model';
        $this->_model = new $modelName();
    }

    protected function _setView($viewName)
    {
        $this->_view = new View(HOME . DS . 'views' . DS . strtolower($this->_modelBaseName) . DS . $viewName . '.tpl');
    }
}