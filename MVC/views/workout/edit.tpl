<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | <?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/style.css?d=<?php echo time(); ?>" />
</head>

<body>
<header>
    <h1>WorkoutLog</h1>
    <h2><?php echo $title; ?></h2>
</header>
<div class="container">
    <h2>Entry: <?php echo $date; ?></h2><br>

    <form action="" method="post">
        <textarea id="entry" name="entry" row="15" cols="50" required>
<?php echo $workoutdata;?></textarea>
        <br><br>
        <button class="btn btn-a" type="submit" name="editFormSubmit">Edit</button>
    </form>
    <a class="btn btn-a" href="/">Return to Index</a>
    <a class="btn btn-a" href="/workout/delete/<?php echo $workoutID; ?>" >Delete entry</a>
</div>
</body>
</html>
