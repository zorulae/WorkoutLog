<!DOCKTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | <?php echo $title; ?></title>
    <!--<link rel="stylesheet" href="style.css">-->
    <link rel="stylesheet" type="text/css" href="style.css?d=<?php echo time(); ?>" />
</head>
<body>
    <header>
        <h1>WorkoutLog</h1>
        <img class="butt-icon" src="https://i.ya-webdesign.com/images/booty-vector-5.png">
        <h2>Track your progress, get your butt up!</h2>
        <div class="container">
            <a class="btn btn-a" href="/workout/save">Add your workout</a>
        </div>
    </header>


<table>
<?php
if($workouts):
foreach ($workouts as $w):
?>
<tr>
<td>
    <a class="btn btn-a entry" href="/workout/edit/<?php echo $w['id']; ?>"></a> 
    <span><?php echo $w['Date'];?></span><br/>
    <?php echo $w['TrainingData'];?>
</td>
</tr>
<?php
endforeach;
else: ?>
</table>


<p>Enter your first workout!</p>
<?php endif; ?>

</body>
</html>