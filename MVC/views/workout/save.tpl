<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | <?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/style.css?d=<?php echo time(); ?>" />
</head>

<body>
    <header>
        <h1>WorkoutLog</h1>
        <img class="butt-icon" src="https://i.ya-webdesign.com/images/booty-vector-5.png">
        <h2><?php echo $title; ?></h2>
    </header>

<?php
if (isset($errors))
{
    echo '<ul>';
    foreach ($errors as $e)
    {
        echo '<li>' . $e . '</li>';
    }
    echo '</ul>';
}
if (isset($saveError))
{
    echo "<h2>Error saving data. Please try again.</h2>" . $saveError;
}
?>

<div class="container">
    <form method="post">
        <textarea id="entry" name="entry" row="15" cols="50" placeholder="Workout goes here" required></textarea>
        <br><br><br>
        <button class="btn btn-a" type="submit" name="addFormSubmit">Submit</button>
    </form>
    <a class="btn btn-a" href="/">Return to Index</a>
</div>
</body>
</html>