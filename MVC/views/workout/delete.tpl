<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | <?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/style.css?d=<?php echo time(); ?>" />
</head>
<body>
    <p>Are you sure you want to delete the following entry?</p>
    <form action="" method="post">
        <button class="btn btn-a" type="submit" name="deleteSubmit" >Yes</button>
    </form>
    <a class="btn btn-a" href="/" >Return to Index</a>
</body>
</html>