<?php

class WorkoutModel extends Model
{

    private $_workoutData;
    private $_id;

    public function setWorkoutData($workoutdata)
    {
        $this->_workoutData = $workoutdata;
    }

    public function getWorkoutData()
    {
        return $this->_workoutData;
    }

    public function getID()
    {
        return $this->_id;
    }

    public function setID($id)
    {
        $this->_id = $id;
    }

    public function saveWorkout()
    {
        $sql = "INSERT INTO log_data VALUES (0, NOW(), ?)";
        $this->_setSql($sql);
        $this->performQuery(array($this->getWorkoutData()));
    }

    public function deleteWorkout()
    {
        $sql = "DELETE from log_data WHERE id=?";
        $this->_setSql($sql);
        $this->performQuery(array($this->getID()));
    }

    public function updateWorkoutData() 
    {
        $sql = "UPDATE log_data SET TrainingData=? WHERE id=?";
        $this->_setSql($sql);
        $this->performQuery(array($this->getWorkoutData(), $this->getID()));
    }

    public function getNWorkouts(int $n=7)
    {
        $sql="SELECT * FROM log_data ORDER BY date DESC LIMIT $n"; 
        $this->_setSql($sql);
        $workouts= $this->getAll(array($n));
        if (empty($workouts))
        {
            return false;
        }
        return $workouts;
    }

    public function getWorkoutById($id)
    {
        $sql = "SELECT * FROM log_data WHERE id=? LIMIT 1";
        $this->setID($id);
        $this->_setSql($sql);
        $workoutdata=$this->getRow(array($this->getID()));

        if (empty($workoutdata))
        {
            return false;
        }
        return $workoutdata;
    }
}