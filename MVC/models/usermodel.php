<?php

class UserModel extends Model
{
    private $_username;
    private $_password;
    private $_userMail;
    private $_id;

    //getUsername
    //getUserMail
    //saveUser
    //deleteUser
    //TODO look up if you even need to have getters and setters in php

    public function setUsername($username)
    {
        $this->_username = $username;
    }
    public function setUserEmail($email)
    {
        $this->_userMail = $email;
    }
    public function setPassword($password) //TODO is it a good practice to do this all at once?
    {
        $this->_password = password_hash($password, PASSWORD_DEFAULT);;
    }

    public function saveUser()
    {
        $sql = "INSERT INTO user_table VALUES (0, ?, ?, ?)";
        $data = array($this->_username, $this->_password, $this->_userMail);
        $this->_setSql($sql);
        $this->performQuery($data);
    }

    public function deleteUser()
    {
        $sql = "DELETE from user_table WHERE id=?";
        $this->_setSql($sql);
        $this->performQuery(array($this->_id));
    }

}