<?php
require_once('db.php');

class WorkoutModel 
{
    public $dbconn;
    //Should I grant this class variables for user input?

    public function __construct()
    {
        $database=new Database();
        $db = Database::init();
        $this->dbconn = $db;
    }

    function getWorkoutModel(int $id)
    {
        $stmt= $this->dbconn->prepare("SELECT * FROM log_data WHERE id='$id' LIMIT 1");
        $stmt->execute();
        return $stmt->fetch();   
    }
    
    function getNLastWorkouts(int $n)
    {
        $stmt= $this->dbconn->prepare("SELECT * FROM log_data ORDER BY date DESC LIMIT '$n'");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC); //Should be like fetch array
    }

}
