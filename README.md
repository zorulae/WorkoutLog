# WorkoutLog

A simple workout logging application for tracking daily exercises and progress.

## Prerequisites
[PHP](https://www.php.net/downloads.php)
[MySQL](https://www.mysql.com/downloads/)
[Apache2](https://httpd.apache.org/download.cgi)

