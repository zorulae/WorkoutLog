<!DOCKTYPE html>
<html>
    
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | Edit Entry</title>
    <link rel="stylesheet" type="text/css" href="style.css?d=<?php echo time(); ?>" />
</head>

<body>
    <header>
        <h1>WorkoutLog</h1>
        <h2>Track your progress, get your butt up</h2>
    </header>

<div class="container">
<?php
    require_once('connectvars.php');
    
    $id=$_GET['id'];

    $dbc= mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
        or die('Error connecting to MySQL server .');
    $textQuery = "SELECT TrainingData, Date FROM log_data WHERE id='$id'";
    $result = mysqli_query($dbc, $textQuery);
    $row=mysqli_fetch_assoc($result);
    $entryData=$row['TrainingData'];
    $date=$row['Date'];
    echo '<p>Entry: ' . $date . '</p>';

    if (isset($_POST['submit'])) {
        $entry = $_POST['entry'];
        $entryData=$entry;

        $query="UPDATE log_data SET TrainingData = '$entry' WHERE id = '$id'";
        mysqli_query($dbc, $query)
            or die('Error querying database.');
        echo '<p>Entry updated.</p>';
    }

    mysqli_close($dbc);
?>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . '?' . 'id=' . $id);?>">
        <textarea id="entry" name="entry" row="15" cols="50" required>
<?php echo $entryData;?></textarea> 
        <br><br>
        <button class="btn btn-a" type="submit" name="submit">Edit</button>
    </form>
    <a class="btn btn-a" href="index.php">Return to Index</a>
    <a class="btn btn-a" href="delete_entry.php?id=<?php echo $id;?>">Delete entry</a>
</div>
</body>

</html>