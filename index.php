<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | Index</title>
    <!--<link rel="stylesheet" href="style.css">-->
    <link rel="stylesheet" type="text/css" href="style.css?d=<?php echo time(); ?>" />
</head>

<body>
    <header>
        <h1>WorkoutLog</h1>
        <img class="butt-icon" src="https://i.ya-webdesign.com/images/booty-vector-5.png">
        <h2>Track your progress, get your butt up!</h2>
        <div class="container">
            <a class="btn btn-a" href="addentry.php">Add your workout</a>
        </div>
    </header>

<?php
    require_once('connectvars.php');

    $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME); 
    $query = "SELECT * FROM log_data ORDER BY date DESC LIMIT 7";
    $data = mysqli_query($dbc, $query);

    echo '<table>';
    while ($row = mysqli_fetch_array($data)) {
        echo '<tr><td>';
        echo '<a class="btn btn-a entry" href="edit_entry.php?' 
            . "id=" . $row['id'] . ' "></a>'; 
        echo '<span> ' . $row['Date'] . '</span><br/>';
        echo $row['TrainingData'] . '</td>';
    }
    echo '</table>';

    mysqli_close($dbc);
?>
</body>
</html>
