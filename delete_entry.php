<!DOCKTYPE html>
<html>
    
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | Delete Entry</title>
    <link rel="stylesheet" type="text/css" href="style.css?d=<?php echo time(); ?>" />
    <!-- Add a style to this html-->
</head>
<body>

<?php
    require_once('connectvars.php');

    $id = $_GET['id'];

    echo '<p>Are you sure you want to delete the following entry?</p>';
    echo '<form method="post" action="'. htmlspecialchars($_SERVER["PHP_SELF"] . '?id=' . $id) . '">';
    echo '<input type="radio" id="Yes" name="confirm" value="Yes" /> ';
    echo '<label for="Yes">Yes</label>';
    echo '<input type="radio" id="No" name="confirm" value="No"  />';
    echo '<label for="No">No</label><br><br>';
    echo '<input class="btn btn-a" type="submit" value="Submit" name="submit" />';
    echo '</form>';

    if (isset($_POST['submit'])) {
        if ($_POST['confirm']=='Yes') {
            $dbc= mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
                or die('Error connecting to MySQL server.');
            $query = "DELETE FROM log_data WHERE id='$id' LIMIT 1";
            mysqli_query($dbc, $query);
            mysqli_close($dbc);
            echo '<p>Entry deleted.</p>';
        }
        else {
            echo '<p>Tough luck.</p>';
        }
        header("Refresh:2; url=index.php");
    }
?>
</body>
</html>

