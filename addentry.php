<!DOCKTYPE html>
<html>
    
<head>
    <meta charset="UTF-8">
    <title>WorkoutLog | Add your training</title>
    <link rel="stylesheet" type="text/css" href="style.css?d=<?php echo time(); ?>" />
</head>

<body>
    <header>
        <h1>WorkoutLog</h1>
        <h2>Track your progress, get your butt up</h2>
    </header>
<div class="container">
<?php
    require_once('connectvars.php');

    if (isset($_POST['submit'])) {
        $entry = $_POST['entry'];

        $dbc= mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
            or die('Error connecting to MySQL server.');
        $query="INSERT INTO log_data VALUES (0, NOW(), '$entry')";
        mysqli_query($dbc, $query)
            or die('Error querying database.');

        $entry = "";

        mysqli_close($dbc);
        echo '<h2>Workout added!</h2>';
        header("Refresh:2; url=index.php");
    }
    else {
        echo '<p class="error">Please enter your finished workout.</p>';
    }
?>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <textarea id="entry" name="entry" row="15" cols="50" placeholder="Workout goes here" required></textarea>
        <br><br>
        <button class="btn btn-a" type="submit" name="submit">Submit</button>
    </form>
    <a class="btn btn-a" href="index.php">Return to Index</a>
    </div>
</body>

</html>